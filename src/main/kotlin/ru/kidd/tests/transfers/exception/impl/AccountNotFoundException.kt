package ru.kidd.tests.transfers.exception.impl

import ru.kidd.tests.transfers.exception.BusinessException

class AccountNotFoundException(accountNum: Long) : BusinessException(
        String.format("Account %s not found", accountNum)
)
