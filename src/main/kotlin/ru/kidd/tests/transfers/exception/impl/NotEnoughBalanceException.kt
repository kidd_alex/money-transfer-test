package ru.kidd.tests.transfers.exception.impl

import ru.kidd.tests.transfers.exception.BusinessException

class NotEnoughBalanceException(accountNum: Long) : BusinessException(
        String.format("Not enough balance on account %s", accountNum)
)
