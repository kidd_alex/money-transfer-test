package ru.kidd.tests.transfers.exception

abstract class BusinessException(message: String) : Exception(message)
