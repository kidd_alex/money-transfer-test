package ru.kidd.tests.transfers.service.impl

import org.springframework.stereotype.Service
import ru.kidd.tests.transfers.domain.Transaction
import ru.kidd.tests.transfers.domain.TransactionType
import ru.kidd.tests.transfers.service.TransactionsService
import java.math.BigDecimal

@Service
class TransactionsServiceImpl : TransactionsService {
    override fun createTransaction(recipient: Long,
                                   amount: BigDecimal,
                                   type: TransactionType): Transaction {
        return Transaction(recipient, amount, type)
    }
}
