package ru.kidd.tests.transfers.service.impl

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.kidd.tests.transfers.dao.AccountsRepository
import ru.kidd.tests.transfers.domain.Account
import ru.kidd.tests.transfers.domain.Transaction
import ru.kidd.tests.transfers.domain.TransactionType
import ru.kidd.tests.transfers.exception.BusinessException
import ru.kidd.tests.transfers.exception.impl.AccountNotFoundException
import ru.kidd.tests.transfers.exception.impl.NotEnoughBalanceException
import ru.kidd.tests.transfers.service.AccountsService
import ru.kidd.tests.transfers.service.TransactionsService
import java.math.BigDecimal

@Service
@Transactional(rollbackFor = [ BusinessException::class ])
class AccountsServiceImpl(
        val repository: AccountsRepository,
        val transactionsService: TransactionsService
) : AccountsService {
    @Throws(BusinessException::class)
    override fun withdraw(accountNum: Long, amount: BigDecimal): Account {
        val account = this.find(accountNum)
        if (checkBalance(account, amount)) {
            val transaction = transactionsService.createTransaction(account.accountNum, amount, TransactionType.WITHDRAW)
            account.balance = account.balance.minus(amount)
            account.transactions.add(transaction)
            return repository.save(account)
        } else {
            throw NotEnoughBalanceException(accountNum)
        }
    }

    @Throws(BusinessException::class)
    override fun deposit(accountNum: Long, amount: BigDecimal): Account {
        val account = this.find(accountNum)
        val transaction = transactionsService.createTransaction(account.accountNum, amount, TransactionType.DEPOSIT)
        account.balance = account.balance.plus(amount)
        account.transactions.add(transaction)
        return repository.save(account)
    }

    @Throws(BusinessException::class)
    override fun transfer(sender: Long, recipient: Long, amount: BigDecimal): Transaction {
        val senderAcc = this.find(sender)
        val recipientAcc = this.find(recipient)

        if (checkBalance(senderAcc, amount)) {
            val transaction = transactionsService.createTransaction(recipientAcc.accountNum, amount, TransactionType.TRANSFER)
            senderAcc.balance = senderAcc.balance.minus(amount)
            senderAcc.transactions.add(transaction)
            recipientAcc.balance = recipientAcc.balance.plus(amount)
            repository.saveAll(listOf(senderAcc, recipientAcc))
            return transaction
        } else {
            throw NotEnoughBalanceException(sender)
        }
    }

    @Transactional(readOnly = true)
    @Throws(BusinessException::class)
    fun find(accountNum: Long): Account {
        val find = repository.findById(accountNum)
        if (find.isPresent) {
            return find.get()
        } else {
            throw AccountNotFoundException(accountNum)
        }
    }

    private fun checkBalance(account: Account, amount: BigDecimal): Boolean = (account.balance.compareTo(amount) != -1)
}
