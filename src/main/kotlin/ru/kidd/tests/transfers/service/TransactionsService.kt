package ru.kidd.tests.transfers.service

import ru.kidd.tests.transfers.domain.Transaction
import ru.kidd.tests.transfers.domain.TransactionType
import java.math.BigDecimal

interface TransactionsService {
    fun createTransaction(recipient: Long, amount: BigDecimal, type: TransactionType): Transaction
}
