package ru.kidd.tests.transfers.service

import ru.kidd.tests.transfers.domain.Account
import ru.kidd.tests.transfers.domain.Transaction
import java.math.BigDecimal

interface AccountsService {
    fun deposit(accountNum: Long, amount: BigDecimal): Account
    fun withdraw(accountNum: Long, amount: BigDecimal): Account
    fun transfer(sender: Long, recipient: Long, amount: BigDecimal): Transaction
}
