package ru.kidd.tests.transfers.configuration

import com.google.common.base.Predicates
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.PathSelectors.ant
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2




@Configuration
@EnableSwagger2
class ApplicationConfig {
    @Bean
    fun restApi(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .select()
                .paths(Predicates.and(ant("/**"), Predicates.not(ant("/error"))))
                .build()
    }
}