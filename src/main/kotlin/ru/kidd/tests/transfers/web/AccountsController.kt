package ru.kidd.tests.transfers.web

import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.*
import ru.kidd.tests.transfers.data.request.AmountDto
import ru.kidd.tests.transfers.data.request.TransferDto
import ru.kidd.tests.transfers.data.response.AccountInfoDto
import ru.kidd.tests.transfers.data.response.ErrorDto
import ru.kidd.tests.transfers.data.response.TransactionInfoDto
import ru.kidd.tests.transfers.exception.impl.AccountNotFoundException
import ru.kidd.tests.transfers.exception.impl.NotEnoughBalanceException
import ru.kidd.tests.transfers.service.AccountsService

@RestControllerAdvice
@RequestMapping("accounts", produces = [(MediaType.APPLICATION_JSON_VALUE)],
        consumes = [(MediaType.APPLICATION_JSON_VALUE)])
class AccountsController(
        val accountsService: AccountsService
) {
    private val logger = LoggerFactory.getLogger(AccountsController::class.java)

    @PostMapping("{accountNum}/withdraw")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Success withdraw"),
        ApiResponse(code = 400, message = "Wrong request params"),
        ApiResponse(code = 404, message = "Account not found"),
        ApiResponse(code = 406, message = "Not enough balance")])
    fun withdraw(@PathVariable("accountNum") accountNum: Long,
                 @RequestBody amountDto: AmountDto): AccountInfoDto {
        val account = accountsService.withdraw(accountNum, amountDto.amount)
        return AccountInfoDto(account.accountNum, account.balance)
    }

    @PostMapping("{accountNum}/deposit")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Success deposit"),
        ApiResponse(code = 400, message = "Wrong request params"),
        ApiResponse(code = 404, message = "Account not found")])
    fun deposit(@PathVariable("accountNum") accountNum: Long,
                @RequestBody amountDto: AmountDto): AccountInfoDto {
        val account = accountsService.deposit(accountNum, amountDto.amount)
        return AccountInfoDto(account.accountNum, account.balance)
    }

    @PostMapping("{accountNum}/transfer")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Success transfer"),
        ApiResponse(code = 400, message = "Wrong request params"),
        ApiResponse(code = 404, message = "Account not found"),
        ApiResponse(code = 406, message = "Not enough balance")])
    fun transfer(@PathVariable("accountNum") accountNum: Long,
                 @RequestBody transferDto: TransferDto): TransactionInfoDto {
        val transaction = accountsService.transfer(accountNum, transferDto.recipient, transferDto.amount)
        return TransactionInfoDto(transaction.id)
    }

    @ExceptionHandler
    fun businessExceptionsHandler(exception: Exception): ResponseEntity<ErrorDto> {
        logger.debug(exception.message, exception)
        return when (exception) {
            is AccountNotFoundException -> ResponseEntity(ErrorDto(exception.message), HttpStatus.NOT_FOUND)
            is NotEnoughBalanceException -> ResponseEntity(ErrorDto(exception.message), HttpStatus.NOT_ACCEPTABLE)
            is HttpMessageNotReadableException -> ResponseEntity(ErrorDto("Incorrect request params"), HttpStatus.BAD_REQUEST)
            else -> ResponseEntity(ErrorDto(exception.message), HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}
