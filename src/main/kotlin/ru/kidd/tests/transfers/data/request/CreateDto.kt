package ru.kidd.tests.transfers.data.request

import java.math.BigDecimal

data class CreateDto(
        val balance: BigDecimal
)
