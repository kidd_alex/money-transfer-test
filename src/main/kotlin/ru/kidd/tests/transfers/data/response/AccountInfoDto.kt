package ru.kidd.tests.transfers.data.response

import java.math.BigDecimal

data class AccountInfoDto(
        val accountNum: Long,
        val balance: BigDecimal
)
