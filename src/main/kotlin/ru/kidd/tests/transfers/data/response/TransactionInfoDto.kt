package ru.kidd.tests.transfers.data.response

data class TransactionInfoDto(
        val transactionId: Long
)
