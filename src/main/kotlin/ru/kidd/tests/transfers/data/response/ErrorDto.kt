package ru.kidd.tests.transfers.data.response

data class ErrorDto(
        val message: String?
)
