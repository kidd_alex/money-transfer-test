package ru.kidd.tests.transfers.data.request

import java.math.BigDecimal

data class TransferDto(
        val recipient: Long,
        val amount: BigDecimal
)
