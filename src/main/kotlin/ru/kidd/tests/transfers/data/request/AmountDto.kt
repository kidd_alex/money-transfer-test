package ru.kidd.tests.transfers.data.request

import java.math.BigDecimal

data class AmountDto(
        val amount: BigDecimal
)
