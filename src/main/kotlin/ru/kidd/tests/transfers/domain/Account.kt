package ru.kidd.tests.transfers.domain

import java.math.BigDecimal
import javax.persistence.*

@Entity
@Table(name = "accounts")
data class Account(
        @Column(name = "balance")
        var balance: BigDecimal = BigDecimal.ZERO,

        @OneToMany(cascade = [ CascadeType.ALL ])
        val transactions: MutableSet<Transaction> = emptySet<Transaction>().toMutableSet(),

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name = "account_num")
        val accountNum: Long = 0
)
