package ru.kidd.tests.transfers.domain

enum class TransactionType {
    WITHDRAW, DEPOSIT, TRANSFER
}
