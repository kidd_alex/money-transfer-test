package ru.kidd.tests.transfers.domain

import java.math.BigDecimal
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "transactions")
data class Transaction(
        @Column(name = "recipient")
        val recipient: Long,

        @Column(name = "amount")
        val amount: BigDecimal,

        @Column(name = "type")
        val type: TransactionType,

        @Column(name = "date")
        val date: LocalDateTime = LocalDateTime.now(),

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0
)
