package ru.kidd.tests.transfers.dao

import org.springframework.data.repository.CrudRepository
import ru.kidd.tests.transfers.domain.Account

interface AccountsRepository : CrudRepository<Account, Long>