package ru.kidd.tests.transfers

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.transaction.annotation.Transactional
import ru.kidd.tests.transfers.dao.AccountsRepository
import ru.kidd.tests.transfers.domain.Account
import ru.kidd.tests.transfers.domain.TransactionType
import ru.kidd.tests.transfers.exception.impl.AccountNotFoundException
import ru.kidd.tests.transfers.exception.impl.NotEnoughBalanceException
import java.math.BigDecimal

@SpringBootTest
@RunWith(SpringRunner::class)
@AutoConfigureMockMvc
@Transactional
class AccountsApiTests {
    @Autowired
    private lateinit var mockMvc: MockMvc
    @Autowired
    private lateinit var accountsRepository: AccountsRepository

    @Before
    fun before() {
        accountsRepository.saveAll(
                listOf(
                        Account(BigDecimal(1000)),
                        Account(BigDecimal(1000))
                )
        )
    }

    @Test
    fun `Test deposit`() {
        val depositRequest = """
            {
                "amount": 2000
            }
            """.trimIndent()

        val accounts = accountsRepository.findAll()

        accounts.forEach {
            val request = post("/accounts/${it.accountNum}/deposit")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(depositRequest)

            mockMvc.perform(request)
                    .andExpect(status().isOk)
                    .andExpect(jsonPath("$.accountNum").value(`is`(it.accountNum), Long::class.java))
                    .andExpect(jsonPath("$.balance").value(`is`(3000)))

            assertTrue("Account must have one transaction", it.transactions.size == 1)

            val transaction = it.transactions.first()
            assertTrue("Transaction amount must be right", transaction.amount == BigDecimal(2000))
            assertTrue("Current account must be recipient", transaction.recipient == it.accountNum)
            assertTrue("Transaction type must be DEPOSIT", transaction.type == TransactionType.DEPOSIT)
        }
    }

    @Test
    fun `Test withdraw`() {
        val withdrawRequest = """
            {
                "amount": 600
            }
            """.trimIndent()

        val account = accountsRepository.findAll().first()

        with(account) {
            val request = post("/accounts/$accountNum/withdraw")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(withdrawRequest)

            mockMvc.perform(request)
                    .andExpect(status().isOk)
                    .andExpect(jsonPath("$.accountNum").value(`is`(accountNum), Long::class.java))
                    .andExpect(jsonPath("$.balance", `is`(400)))
        }
    }

    @Test
    fun `Test overdraft`() {
        val withdrawRequest = """
            {
                "amount": 1200
            }
            """.trimIndent()

        val account = accountsRepository.findAll().first()

        with(account) {
            val request = post("/accounts/$accountNum/withdraw")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(withdrawRequest)

            // trying to make negative balance
            mockMvc.perform(request)
                    .andExpect(status().isNotAcceptable)
                    .andExpect(jsonPath("$.message", `is`(NotEnoughBalanceException(accountNum).message)))
        }
    }

    @Test
    fun `Test account doesn't exist`() {
        val withdrawRequest = """
            {
                "amount": 600
            }
            """.trimIndent()

        val accountNum = Long.MAX_VALUE
        val request = post("/accounts/$accountNum/withdraw")
                .contentType(MediaType.APPLICATION_JSON)
                .content(withdrawRequest)

        mockMvc.perform(request)
                .andExpect(status().isNotFound)
                .andExpect(jsonPath("$.message", `is`(AccountNotFoundException(accountNum).message)))
    }

    @Test
    fun `Test transfer`() {
        val accounts = accountsRepository.findAll()
        val recipient = accounts.last()
        val transferRequest = """
            {
                "recipient": ${recipient.accountNum},
                "amount": 600
            }
            """.trimIndent()

        val sender = accounts.first()
        with(sender) {
            val request = post("/accounts/$accountNum/transfer")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(transferRequest)

            mockMvc.perform(request)
                    .andExpect(status().isOk)
                    .andExpect(jsonPath("$.transactionId", notNullValue()))

            assertTrue("Sender's balance must have been reduced", sender.balance == BigDecimal(400))
            assertTrue("Recipient's balance must have been increased", recipient.balance == BigDecimal(1600))

            val transaction = sender.transactions.first()
            assertTrue("Transaction amount must be right", transaction.amount == BigDecimal(600))
            assertTrue("Current account must not be recipient", transaction.recipient != sender.accountNum)
            assertTrue("Transaction type must be TRANSFER", transaction.type == TransactionType.TRANSFER)
        }
    }
}
